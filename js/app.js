document.addEventListener("DOMContentLoaded", function() {

    const mockApiUrl = 'http://localhost:3000/';

    const mainNavigation = document.querySelector('#mainNavigation');
    fetch(mockApiUrl + 'menu').then((response) => response.json()).then(function(data) {
        mainNavigation.innerHTML = '';
        data.map((menuItemData) => {
            let item = document.createElement('a');
            item.href = menuItemData.url;
            item.title = menuItemData.title;
            item.classList.add('nav-link');
            item.innerHTML = menuItemData.title;
            mainNavigation.appendChild(item);
        });
    }).catch(function (error) {
        console.log(error);
    });

    const productsList = document.getElementById('productsList');
    const productsListObserver = new MutationObserver(initEditing);
    productsListObserver.observe(productsList, {childList: true});
    fetch(mockApiUrl + 'products').then((response) => response.json()).then(function(data) {
        let output = data.map((productsData) => {
            return `
            <li class="products-list__item">
                <a 
                    href="/product-${productsData.id}" 
                    class="products-list__item-link" 
                    title="${productsData.title}"
                    data-product-id="${productsData.id}"
                >
                    <img src="images/products/${productsData.image}" class="products-list__item-image" alt="${productsData.title}">
                    <div class="products-list__item-name">${productsData.name}</div>
                </a>
            </li>`;
        });
        productsList.innerHTML = output.join('');
    }).catch(function (error) {
        console.log(error);
    });

    const carouselSlides = document.getElementById('carouselSlides');
    fetch(mockApiUrl + 'products?featured=true').then((response) => response.json()).then(function(data) {
        let output = data.map((carouselData) => {
            return `
            <div class="carousel__slide">
                <img src="images/carousel/${carouselData.image}" alt="${carouselData.title}" class="carousel__slide-image">
                <a href="/product-${carouselData.id}" class="carousel__slide-link" title="${carouselData.title}">
                    <div class="carousel__slide-content">
                        <div class="carousel__slide-header">${carouselData.title}</div>
                        ${carouselData.body}
                    </div>
                </a>
            </div>`;
        });
        carouselSlides.innerHTML = output.join('');
        carouselSlides.firstElementChild.classList.add('active');
        carouselSlides.firstElementChild.nextElementSibling.classList.add('right');
        carouselSlides.lastElementChild.classList.add('left');
    }).catch(function (error) {
        console.log(error);
    });

    let carousel = document.querySelector('[data-carousel]');
    carousel.querySelector('.carousel_arrow--left').addEventListener('click', slideLeft);
    carousel.querySelector('.carousel_arrow--right').addEventListener('click', slideRight);

    function slideLeft(e) {
        e.preventDefault();
        let slideActive = carousel.querySelector('.carousel__track .active');
        let slideLeft = carousel.querySelector('.carousel__track .left');
        let slideRight = carousel.querySelector('.carousel__track .right');
        slideLeft.classList.remove('left');
        slideActive.classList.replace('active', 'left');
        slideRight.classList.replace('right', 'active');

        slideActive.addEventListener('transitionend', function() {
            if (slideRight.nextElementSibling) {
                slideRight.nextElementSibling.classList.add('right');
            } else {
                slideRight.parentNode.firstElementChild.classList.add('right');
            }
        }, { once: true });
    }

    function slideRight(e) {
        e.preventDefault();
        let slideActive = carousel.querySelector('.carousel__track .active');
        let slideLeft = carousel.querySelector('.carousel__track .left');
        let slideRight = carousel.querySelector('.carousel__track .right');
        slideLeft.classList.replace('left', 'active');
        slideActive.classList.replace('active', 'right');
        slideRight.classList.remove('right');

        slideActive.addEventListener('transitionend', function() {
            if (slideLeft.previousElementSibling) {
                slideLeft.previousElementSibling.classList.add('left');
            } else {
                slideLeft.parentNode.lastElementChild.classList.add('left');
            }
        }, { once: true });
    }

    document.querySelector('#addNewProductTrigger').addEventListener('click', openAddNewProduct);
    document.querySelector('#addNewProduct').addEventListener('click', addNewProduct);
    document.querySelector('#removeProduct').addEventListener('click', removeProduct);
    document.querySelector('#closeProductForm').addEventListener('click', (e) => {
        e.preventDefault();
        document.querySelector('#addNewProductForm').style.display = 'none';
    });

    function openAddNewProduct(e) {
        e.preventDefault();
        document.querySelector('#addNewProductForm').style.display = 'flex';
        document.getElementById('newAction').value = 'create';
        document.getElementById('updateId').value = '';
        document.getElementById('newImage').value = '';
        document.getElementById('newName').value = '';
        document.getElementById('newTitle').value = '';
        document.getElementById('newDescription').value = '';
        document.getElementById('newFeatured').checked = false;
        document.getElementById('removeProduct').style.display = 'none';
    }

    function addNewProduct(e) {
        e.preventDefault();
        let name = document.getElementById('newName');
        let title = document.getElementById('newTitle');
        let desc = document.getElementById('newDescription');
        let featured = document.getElementById('newFeatured');
        let image = document.getElementById('newImage');
        let action = document.getElementById('newAction');
        let updateId = document.getElementById('updateId');

        const checkNotEmpty = (el) => {
            let valid = false;
            if (el.value.trim() === '') {
                el.classList.add('error');
            } else {
                valid = true;
                el.classList.remove('error');
            }
            return valid;
        }

        let isNameValid = checkNotEmpty(name),
            isTitleValid = checkNotEmpty(title),
            isDescValid = checkNotEmpty(desc);

        let isFormValid = isNameValid &&
            isTitleValid &&
            isDescValid;

        if (isFormValid && action.value === 'create') {
            let image = Math.floor(Math.random() * (8 - 1) + 1);
            let data = {
                "featured": featured.checked,
                "image": image + ".jpg",
                "name": name.value,
                "title": title.value,
                "body": desc.value
            };
            let fetchData = {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                }
            }
            fetch(mockApiUrl + 'products', fetchData)
                .then(response => response.json())
                .then(function(ok) {
                    console.log(ok);
                    location.reload();
                }).catch(function (error) {
                console.log(error);
            });
        }

        if (isFormValid && action.value === 'update') {
            let data = {
                "featured": featured.checked,
                "name": name.value,
                "title": title.value,
                "body": desc.value,
                "image": image.value
            };
            let fetchData = {
                method: 'PUT',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                }
            }
            fetch(mockApiUrl + 'products/' + updateId.value, fetchData)
                .then(response => response.json())
                .then(function(ok) {
                    console.log(ok);
                    location.reload();
                }).catch(function (error) {
                console.log(error);
            });
        }
    }

    function initEditing(mutationsList) {
        for(const mutation of mutationsList) {
            if (mutation.type === 'childList') {
                const products = productsList.querySelectorAll('.products-list__item-link');
                products.forEach(product => {
                    product.addEventListener('click', function (e) {
                        e.preventDefault();
                        let productId = this.getAttribute('data-product-id');
                        openEditProduct(productId);
                    });
                });
            }
        }
    }

    function openEditProduct(productId) {
        fetch(mockApiUrl + 'products/' + productId).then((response) => response.json()).then(function(data) {
            //console.log(data);
            //open form
            document.querySelector('#addNewProductForm').style.display = 'flex';
            //fill in data in form
            document.getElementById('newName').value = data.name;
            document.getElementById('newTitle').value = data.title;
            document.getElementById('newDescription').value = data.body;
            document.getElementById('newFeatured').checked = data.featured;
            document.getElementById('newImage').value = data.image;
            //set action
            document.getElementById('newAction').value = 'update';
            //set id
            document.getElementById('updateId').value = productId;
            //show delete btn
            document.getElementById('removeProduct').style.display = 'block';
        }).catch(function (error) {
            console.log(error);
        });
    }

    function removeProduct(e) {
        e.preventDefault();
        let updateId = document.getElementById('updateId');
        if (confirm('Sure?')) {
            let fetchData = {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                }
            }
            fetch(mockApiUrl + 'products/' + updateId.value, fetchData)
                .then(response => response.json())
                .then(function(ok) {
                    console.log(ok);
                    location.reload();
                }).catch(function (error) {
                console.log(error);
            });
        }
    }

});
